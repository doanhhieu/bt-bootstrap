function checkEmail() {
  filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
  if (filter.test($('#email').val()))
    return true;
}

function validateEmail() {
  if (checkEmail()) {
    $('#email').removeClass('error');
    $('#alert-email').text('');
    $('#alert-email').removeClass('error');
    return true;
  } 
  else {
    $('#email').addClass('error');
    $('#alert-email').text('* Địa chỉ email không hợp lệ');
    $('#alert-email').addClass('error');
    return false;
  }
}

function checkNoidung() {
  filter = /^[a-zA-Z.]/;
  if (filter.test($('#comment').val()))
    return true;
}

function validateNoidung() {
  if (checkCompany()) {
    $('#comment').removeClass('error');
    $('#alert-comment').text('');
    $('#alert-comment').removeClass('error');
    return true;
  } 
  else {
    $('#comment').addClass('error');
    $('#alert-comment').text('* Tên công ty không hợp lệ');
    $('#alert-comment').addClass('error');
    return false;
    }
}
